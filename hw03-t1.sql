CREATE OR REPLACE TRIGGER OF_Switching
AFTER INSERT ON STATE_CHANGE
FOR EACH ROW
WHEN (NEW.ChangeType = 'O' OR NEW.ChangeType = 'F')
DECLARE
	CellN NUMBER;
BEGIN
	-- ricerca del CellId per modifiche del CurrentPhone#
	SELECT CellId INTO CellN
	FROM CELL
	WHERE :NEW.x >= x0 AND :NEW.x < x1 AND
			:NEW.y >= y0 AND :NEW.y < y1;

	IF(:NEW.ChangeType = 'F') THEN
		-- telefono on -> off, rimozione da TELEPHONE
		DELETE FROM TELEPHONE
		WHERE PhoneNo = :NEW.PhoneNo;
		-- aggiornamento numero telefoni collegati alla cella
		UPDATE CELL
		SET CurrentPhone# = CurrentPhone# - 1
		WHERE CellId = CellN;
	ELSE
		-- telefono off -> one, inserimento in TELEPHONE
		INSERT INTO TELEPHONE(PhoneNo, x, y, PhoneState)
		VALUES(:NEW.PhoneNo, :NEW.x, :NEW.y, 'On');
		-- aggiornamento numero telefoni collegati alla cella
		UPDATE CELL
		SET CurrentPhone# = CurrentPhone# + 1
		WHERE CellId = CellN;
	END IF;
END;
/
