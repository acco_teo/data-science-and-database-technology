CREATE OR REPLACE TRIGGER GoS
AFTER UPDATE OF MaxCalls ON CELL
DECLARE
	MaxCalls_Sum NUMBER;
BEGIN
	-- calcolo della somma del massimo delle chiamate
	SELECT SUM(MaxCalls) INTO MaxCalls_Sum
	FROM CELL;

	IF(MaxCalls_Sum <= 30) THEN
		-- violazione del vincolo sul massimo delle chiamate
		RAISE_APPLICATION_ERROR(-20020,
			'Error! The complessive number of calls can not be less than 30.');
	END IF;
END;
/
