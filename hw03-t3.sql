CREATE OR REPLACE TRIGGER MaxCalls_Changing
BEFORE UPDATE OF MaxCalls ON CELL
FOR EACH ROW
WHEN (NEW.MaxCalls < OLD.MaxCalls)
DECLARE
	Curr_CallN NUMBER;
BEGIN
	-- conteggio delle chiamate attive al momento della modifica
	SELECT COUNT(PhoneNo) INTO Curr_CallN
	FROM TELEPHONE
	WHERE :NEW.x0 <= x AND :NEW.x1 > x AND
				:NEW.y0 <= y AND :NEW.y1 > y AND
				PhoneState = 'Active';

	IF(Curr_CallN > :NEW.MaxCalls) THEN
		-- il numero di chiamate in corso > MaxCalls
  		:NEW.MaxCalls := Curr_CallN;
	END IF;
END;
/
