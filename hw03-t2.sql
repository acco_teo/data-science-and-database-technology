CREATE OR REPLACE TRIGGER C_Switching
AFTER INSERT ON STATE_CHANGE
FOR EACH ROW
WHEN (NEW.ChangeType = 'C')
DECLARE
	CellN NUMBER;
	Max_CallN NUMBER;
	isOn NUMBER;
	xc0 NUMBER;
	yc0 NUMBER;
	xc1 NUMBER;
	yc1 NUMBER;
	Curr_CallN NUMBER;
	ExN NUMBER;
BEGIN
	-- il telefono per poter effettuare una chiamata deve essere On
	SELECT COUNT(PhoneNo) INTO isOn
	FROM TELEPHONE
	WHERE PhoneNo = :NEW.PhoneNo AND PhoneState = 'On';

	IF (isOn = 0) THEN
		-- violazione vincolo si suppone che il passaggio di
		-- cella modifichi solo le coordinate (x,y) della tupla
		-- ma non il suo stato, necessario il rollback
		RAISE_APPLICATION_ERROR(-20020,
			'Error! Cannot call if the phone is not On.');
	END IF;

	-- ricerca del CellId e del massimo numero di chiamate attive
	-- necessario salvare le coordinate per contare il numero di
	-- chiamate in corso
	SELECT CellId, MaxCalls, x0, y0, x1, y1
	INTO CellN, Max_CallN, xc0, yc0, xc1, yc1
	FROM CELL
	WHERE x0 <= :NEW.x AND x1 > :NEW.x AND
			y0 <= :NEW.y AND y1 > :NEW.y;

	-- conteggio chiamate in corso nella cella
	SELECT COUNT(PhoneNo) INTO Curr_CallN
	FROM TELEPHONE
	WHERE PhoneState = 'Active' AND
			xc0 <= x AND xc1 > x AND
			yc0 <= y AND yc1 > y;

	IF (Curr_CallN < Max_CallN) THEN
		-- chiamata possibile, aggiornamento stato
		UPDATE TELEPHONE
    SET PhoneState = 'Active'
    WHERE PhoneNo = :NEW.PhoneNo;

	ELSE
		-- chiamata non possibile, inserimento EXCEPTION_LOG
		-- ricerca dell'ultimo ExId relativo alla cella
		SELECT MAX(ExId) INTO ExN
		FROM EXCEPTION_LOG
		WHERE CellID = CellN;

			-- non esistono segnalazioni relative alla cella in esame
			IF (ExN IS NULL) THEN
				ExN := 0;
			END IF;

			INSERT INTO EXCEPTION_LOG(ExId, CellId, ExceptionType)
			VALUES(ExN + 1, CellN, 'V');
	END IF;
END;
/
